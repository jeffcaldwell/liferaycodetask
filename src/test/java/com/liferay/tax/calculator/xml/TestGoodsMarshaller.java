package com.liferay.tax.calculator.xml;

import java.io.File;
import javax.xml.bind.JAXBException;

import com.liferay.tax.calculator.xml.jaxb.Taxcalculator;

public class TestGoodsMarshaller {
	private String projectDirectory = "C:\\Users\\jeff\\eclipse-workspace\\LiferayCodeTask";
	
	public static void main(String[] args) {
		TestGoodsMarshaller tgm = new TestGoodsMarshaller();
		tgm.testMarshaller();
	}
	
	public void testMarshaller() {
		File xsdfile = new File(projectDirectory + "\\src\\main\\java\\com\\liferay\\tax\\calculator\\xml\\xsd\\Goods.xsd");
		File xmlfile = new File(projectDirectory + "\\Goods.xml");
		GoodsMarshaller goodsMarshaller = new GoodsMarshaller();
		try {
			Taxcalculator taxcalculator = goodsMarshaller.unmarshall(xsdfile, xmlfile);
			for (Taxcalculator.Goods.Good good : taxcalculator.getGoods().getGood()) {
				System.out.println(good.getName());
			}
		} catch ( JAXBException e) {
			e.printStackTrace();
		}
	}
}
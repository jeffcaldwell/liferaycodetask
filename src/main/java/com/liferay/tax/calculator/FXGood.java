package com.liferay.tax.calculator;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class FXGood {
	private SimpleIntegerProperty count;
    private SimpleStringProperty  name;
    private SimpleDoubleProperty  price;
    private SimpleStringProperty  description;
    private SimpleBooleanProperty isImported;
    private SimpleDoubleProperty  calculatedPrice;
    
    public FXGood(Integer c, String n, Double p, Double cp, String desc, Boolean isImp) {
    	count = new SimpleIntegerProperty(c);
    	name  = new SimpleStringProperty(n);
    	price = new SimpleDoubleProperty(p);
    	calculatedPrice = new SimpleDoubleProperty(p);
    	description = new SimpleStringProperty(desc);
    	isImported = new SimpleBooleanProperty(isImp);
    }

    public Integer getCount() {
    	return count.get();
    }    
    
    public void setCount(SimpleIntegerProperty c) {
    	count = c;
    }
    
    public void setCount(Integer c) {
    	count = new SimpleIntegerProperty(c);
    }
    
	public String getName() {
		return name.get();
	}

	public void setName(SimpleStringProperty name) {
		this.name = name;
	}

	public Double getPrice() {
		return price.get();
	}

	public void setPrice(SimpleDoubleProperty price) {
		this.price = price;
	}

	public Double getCalculatedPrice() {
		return calculatedPrice.get();
	}

	public void setCalculatedPrice(SimpleDoubleProperty price) {
		this.calculatedPrice = price;
	}
	
	public void setCalculatedPrice(Double price) {
		this.calculatedPrice = new SimpleDoubleProperty(price);
	}
	
	public String getDescription() {
		return description.get();
	}
	
	public void setDescription(SimpleStringProperty description) {
		this.description = description;
	}

	public boolean getIsImported() {
		return isImported.get();
	}

	public void setIsImported(SimpleBooleanProperty isImported) {
		this.isImported = isImported;
	}
}
package com.liferay.tax.calculator.xml;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.Reader;
import com.liferay.tax.calculator.xml.jaxb.Taxcalculator;

public class GoodsMarshaller {

	public Taxcalculator unmarshall(File goodsXSDFile, File goodsXMLFile) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Taxcalculator.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Taxcalculator taxcalculator = (Taxcalculator) unmarshaller.unmarshal(goodsXMLFile);
		return taxcalculator;
	}

	public Taxcalculator unmarshall(Reader goodsXMLFile) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Taxcalculator.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Taxcalculator taxcalculator = (Taxcalculator) unmarshaller.unmarshal(goodsXMLFile);
		return taxcalculator;
	}
	
	public void marshall(Taxcalculator taxcalculator, File goodsXMLFile) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Taxcalculator.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(taxcalculator, goodsXMLFile);
			marshaller.marshal(taxcalculator, System.out);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
}
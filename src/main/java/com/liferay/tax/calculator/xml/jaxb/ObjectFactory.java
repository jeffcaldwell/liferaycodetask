//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0-b170531.0717 
//         See <a href="https://jaxb.java.net/">https://jaxb.java.net/</a> 
//         Any modifications to this file will be lost upon recompilation of the source schema. 
//         Generated on: 2018.03.24 at 11:09:25 PM EDT 
//


package com.liferay.tax.calculator.xml.jaxb;

import javax.xml.bind.annotation.XmlRegistry;

import com.liferay.tax.calculator.xml.jaxb.Taxcalculator.Goods;
import com.liferay.tax.calculator.xml.jaxb.Taxcalculator.Taxrate;
import com.liferay.tax.calculator.xml.jaxb.Taxcalculator.Goods.Good;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.liferay package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.liferay
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Taxcalculator }
     * 
     */
    public Taxcalculator createTaxcalculator() {
        return new Taxcalculator();
    }

    /**
     * Create an instance of {@link Taxcalculator.Goods }
     * 
     */
    public Taxcalculator.Goods createTaxcalculatorGoods() {
        return new Taxcalculator.Goods();
    }

    /**
     * Create an instance of {@link Taxcalculator.Taxrate }
     * 
     */
    public Taxcalculator.Taxrate createTaxcalculatorTaxrate() {
        return new Taxcalculator.Taxrate();
    }

    /**
     * Create an instance of {@link Taxcalculator.Goods.Good }
     * 
     */
    public Taxcalculator.Goods.Good createTaxcalculatorGoodsGood() {
        return new Taxcalculator.Goods.Good();
    }

}

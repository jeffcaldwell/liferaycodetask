package com.liferay.tax.calculator.xml.gui;

import com.liferay.tax.calculator.FXGood;
import javafx.collections.ObservableList;
import com.liferay.tax.calculator.xml.jaxb.Taxcalculator.Taxrate;

public class PurchaseStateModel {
	private Taxrate taxrate;
	private ObservableList<FXGood> cartObserverableList;
	
	public PurchaseStateModel(ObservableList<FXGood> observerbleList) {
		cartObserverableList = observerbleList;
	}
	
	public javafx.collections.ObservableList<FXGood> getObservableList() {
		return cartObserverableList;
	}
	
	public void addTaxRate(Taxrate tr) {
		taxrate = tr;
	}
	
	public void addToCart(FXGood fxgood) {
		if(cartObserverableList.contains(fxgood)) {
			incrementCount(fxgood);
		}else {
			fxgood.setCalculatedPrice(calculatePriceWithTax(fxgood));
			cartObserverableList.add(fxgood);
		}
	}
	
	private synchronized void incrementCount(FXGood newgood) {
		if(cartObserverableList.contains(newgood)) {
			int index = cartObserverableList.indexOf(newgood);
			FXGood good = cartObserverableList.get(index);
			int count = good.getCount();
			good.setCount(Integer.valueOf(count + 1));
			good.setCalculatedPrice(calculatePriceWithTax(good));
		}
	}
	
	private Double calculatePriceWithTax(FXGood good) {
		boolean isImported = good.getIsImported();
		double price = good.getPrice().doubleValue();
		double newprice = (price * taxrate.getSalestax()) + price;
		newprice = (good.getCount() * newprice);
		if(isImported) {
			newprice = newprice + (good.getCount() * (price * taxrate.getInportedsalestax()));
		}
		return Double.valueOf(newprice);
	}
	
	public void deleteFromCart(FXGood fxgood) {
		if(cartObserverableList.contains(fxgood)) {
			decrementCount(fxgood);
		}
	}
	
	private synchronized void decrementCount(FXGood newgood) {
		if(cartObserverableList.contains(newgood)) {
			int index = cartObserverableList.indexOf(newgood);
			FXGood good = cartObserverableList.get(index);
			int count = good.getCount();
			if(count > 1) {
				good.setCount(count - 1);
				good.setCalculatedPrice(calculatePriceWithTax(good));
			}else {
				cartObserverableList.remove(index);
			}
		}
	}
	
	public Double getReceiptTotal() {
		double total = 0.0;
		for(FXGood good:cartObserverableList ) {
			total += good.getCalculatedPrice();
		}
		return total;
	}
}
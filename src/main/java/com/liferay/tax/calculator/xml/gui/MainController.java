package com.liferay.tax.calculator.xml.gui;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import com.liferay.tax.calculator.FXGood;
import com.liferay.tax.calculator.TaxGoodsCalculator;
import com.liferay.tax.calculator.xml.jaxb.Taxcalculator;
import com.liferay.tax.calculator.xml.jaxb.Taxcalculator.Goods;
import com.liferay.tax.calculator.xml.jaxb.Taxcalculator.Goods.Good;
import com.liferay.tax.calculator.xml.jaxb.Taxcalculator.Taxrate;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class MainController {
	//  GOODS STORE TABLE VIEW
	@FXML private TableView<FXGood> GoodsStoreTableViewId;
	@FXML private TableColumn<FXGood,String>  StoreGoodColumnId;
	@FXML private TableColumn<FXGood,Double>  StorePriceColumnId;
	@FXML private TableColumn<FXGood,Boolean> isImportedColumnId;
	// CART TABLE VIEW
	@FXML private TableView<FXGood> CartTableViewId;
	@FXML private TableColumn<FXGood,Integer> CartCountColumnId;
	@FXML private TableColumn<FXGood,String>  CartGoodColumnId;
	@FXML private TableColumn<FXGood,Double>  CartPriceColumnId;
	@FXML private TableColumn<FXGood,Double>  CartCalPriceColumnId;
	// RECEIPT TABLE VIEW
	@FXML private TableView<FXGood> receiptTableViewId;
	@FXML private TableColumn<FXGood,Integer>  ReceiptCountColumnId;
	@FXML private TableColumn<FXGood,String>   ReceiptGoodColumnId;
	@FXML private TableColumn<FXGood,Double>   ReceiptPriceColumnId;
	@FXML private TableColumn<FXGood,Double>   ReceiptCalPriceColumnId;
	
	@FXML private Label TotalId;
	@FXML private Button AddItemButtonId;
	@FXML private Button DeleteItemButton;
	@FXML private Button CheckOutButtonId;
	
	private TaxGoodsCalculator taxCalculator;
	private PurchaseStateModel purchaseStateModel;
	private FXGood selectedGood;
	private FXGood selectedCartGood;
	private ObservableList<FXGood> goodsObservableList    = FXCollections.observableArrayList();
	private ObservableList<FXGood> cartObserverbleList    = FXCollections.observableArrayList();
	private ObservableList<FXGood> receiptObserverbleList = FXCollections.observableArrayList();
	private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
	
	public MainController() {
		purchaseStateModel = new PurchaseStateModel(cartObserverbleList);
		taxCalculator = new TaxGoodsCalculator();
		Taxrate taxrate = taxCalculator.getTaxCalculator().getTaxrate();
		purchaseStateModel.addTaxRate(taxrate);
	}
	
    @FXML private void initialize() {
    	System.out.println("initialie MainController");
    	initGoodsTable();
    	initCartTable();
    	initReceiptTable();
    	intiButtons();
    }
	
	@FXML
	public void GoodsTableOnMouseClick() {
		System.out.println("Goods Table Mouse Clicked");
		selectedGood = GoodsStoreTableViewId.getSelectionModel().getSelectedItem();
		System.out.println("Selected.." + selectedGood.getName());
	}
	
	@FXML
	public void CartTableOnMouseClick() {
		System.out.println("Goods Table Mouse Clicked");
		selectedCartGood = CartTableViewId.getSelectionModel().getSelectedItem();
	}
	
	private void initGoodsTable() {
        //"Name" column
        StoreGoodColumnId.setText("Name");
        StoreGoodColumnId.setCellValueFactory(new PropertyValueFactory<FXGood,String>("name"));
        //"Price" column
        StorePriceColumnId.setText("Price");
        StorePriceColumnId.setCellValueFactory(new PropertyValueFactory<FXGood,Double>("price"));
        StorePriceColumnId.setCellFactory(tc -> new TableCell<FXGood, Double>() {
	        @Override
	        protected void updateItem(Double price, boolean empty) {
	            super.updateItem(price, empty);
	            if (empty) {
	                setText(null);
	            } else {
	                setText(currencyFormat.format(price));
	            }
	        }
	    });	
        
        isImportedColumnId.setText("Imported");
        isImportedColumnId.setCellValueFactory(new PropertyValueFactory<FXGood,Boolean>("IsImported"));
        
        List<FXGood> fxgoodsList =  getGoodsAndConvert();
        GoodsStoreTableViewId.setItems(goodsObservableList);     
        goodsObservableList.addAll(fxgoodsList);
	}
	
	private void initCartTable() {
        //"Count" column
		CartCountColumnId.setText("Count");
		CartCountColumnId.setCellValueFactory(new PropertyValueFactory<FXGood,Integer>("count"));
        //"Good" column
		CartGoodColumnId.setText("Good");
		CartGoodColumnId.setCellValueFactory(new PropertyValueFactory<FXGood,String>("name"));
        //"Price" column
		CartPriceColumnId.setText("Price");
		CartPriceColumnId.setCellValueFactory(new PropertyValueFactory<FXGood,Double>("Price"));
		CartPriceColumnId.setCellFactory(tc -> new TableCell<FXGood, Double>() {
	        @Override
	        protected void updateItem(Double price, boolean empty) {
	            super.updateItem(price, empty);
	            if (empty) {
	                setText(null);
	            } else {
	                setText(currencyFormat.format(price));
	            }
	        }
	    });		
		// calculated price column
		CartCalPriceColumnId.setText("Calculated Price");
		CartCalPriceColumnId.setCellValueFactory(new PropertyValueFactory<FXGood,Double>("calculatedPrice"));
		CartCalPriceColumnId.setCellFactory(tc -> new TableCell<FXGood, Double>() {
	        @Override
	        protected void updateItem(Double price, boolean empty) {
	            super.updateItem(price, empty);
	            if (empty) {
	                setText(null);
	            } else {
	                setText(currencyFormat.format(price));
	            }
	        }
	    });
	    
		CartTableViewId.setItems( cartObserverbleList );
	}
	
	private void initReceiptTable() {
		NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
		//TableView
		receiptTableViewId.setItems(receiptObserverbleList);
		// COUNT column
		ReceiptCountColumnId.setText("CNT");
		ReceiptCountColumnId.setCellValueFactory(new PropertyValueFactory<FXGood,Integer>("count"));
		// GOOD NAME column
	    ReceiptGoodColumnId.setText("Name");
	    ReceiptGoodColumnId.setCellValueFactory(new PropertyValueFactory<FXGood,String>("Name"));
	    // PRICE column
	    ReceiptPriceColumnId.setText("Price");
	    ReceiptPriceColumnId.setCellValueFactory(new PropertyValueFactory<FXGood,Double>("Price"));
	    ReceiptPriceColumnId.setCellFactory(tc -> new TableCell<FXGood, Double>() {
	        @Override
	        protected void updateItem(Double price, boolean empty) {
	            super.updateItem(price, empty);
	            if (empty) {
	                setText(null);
	            } else {
	                setText(currencyFormat.format(price));
	            }
	        }
	    });
	    
	    // CALCULATED PRICE column
	    ReceiptCalPriceColumnId.setText("Calculated Price");
	    ReceiptCalPriceColumnId.setCellValueFactory(new PropertyValueFactory<FXGood,Double>("CalculatedPrice"));
	    ReceiptCalPriceColumnId.setCellFactory(tc -> new TableCell<FXGood, Double>() {
	        @Override
	        protected void updateItem(Double price, boolean empty) {
	            super.updateItem(price, empty);
	            if (empty) {
	                setText(null);
	            } else {
	                setText(currencyFormat.format(price));
	            }
	        }
	    });
	    
	}
	
	private void intiButtons() {
		AddItemButtonId.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	            System.out.println("Add Button Pressed" );
	            purchaseStateModel.addToCart(selectedGood);
	            GoodsStoreTableViewId.getSelectionModel().clearSelection();
	            CartTableViewId.refresh();
	            receiptObserverbleList.clear();
	        }
	    });
		
		DeleteItemButton.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	            System.out.println("Delete Pressed" );
	            if(selectedCartGood != null) {
		            purchaseStateModel.deleteFromCart(selectedCartGood);
		            CartTableViewId.getSelectionModel().clearSelection();
		            CartTableViewId.refresh();
		            receiptObserverbleList.clear();
		            TotalId.setText("");
	            }
	        }
	    });
	
		CheckOutButtonId.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	        	receiptObserverbleList.clear();
	        	receiptObserverbleList.addAll(purchaseStateModel.getObservableList());
	            TotalId.setText( currencyFormat.format(purchaseStateModel.getReceiptTotal()));
	        }
	    });
	}
	
	private List<FXGood> getGoodsAndConvert(){
		com.liferay.tax.calculator.xml.jaxb.Taxcalculator tc = taxCalculator.getTaxCalculator();
        Goods goods = tc.getGoods();
        List<Taxcalculator.Goods.Good> goodList = goods.getGood();
        List<FXGood> fxgoodsList = new ArrayList<FXGood>();
        for(Good good:goodList) {
        	FXGood fxgood = new FXGood( Integer.valueOf(1),good.getName(),good.getPrice(),0.0,good.getDescription(), good.isIsImported());
        	fxgoodsList.add(fxgood);
        }
        return fxgoodsList;
	}
}
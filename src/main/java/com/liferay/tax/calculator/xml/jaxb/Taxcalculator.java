//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0-b170531.0717 
//         See <a href="https://jaxb.java.net/">https://jaxb.java.net/</a> 
//         Any modifications to this file will be lost upon recompilation of the source schema. 
//         Generated on: 2018.03.24 at 11:09:25 PM EDT 
//


package com.liferay.tax.calculator.xml.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="taxrate"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="salestax" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *                   &lt;element name="inportedsalestax" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="goods"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="good" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *                             &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="isImported" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "taxrate",
    "goods"
})
@XmlRootElement(name = "taxcalculator")
public class Taxcalculator {

    @XmlElement(required = true)
    protected Taxcalculator.Taxrate taxrate;
    @XmlElement(required = true)
    protected Taxcalculator.Goods goods;

    /**
     * Gets the value of the taxrate property.
     * 
     * @return
     *     possible object is
     *     {@link Taxcalculator.Taxrate }
     *     
     */
    public Taxcalculator.Taxrate getTaxrate() {
        return taxrate;
    }

    /**
     * Sets the value of the taxrate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Taxcalculator.Taxrate }
     *     
     */
    public void setTaxrate(Taxcalculator.Taxrate value) {
        this.taxrate = value;
    }

    /**
     * Gets the value of the goods property.
     * 
     * @return
     *     possible object is
     *     {@link Taxcalculator.Goods }
     *     
     */
    public Taxcalculator.Goods getGoods() {
        return goods;
    }

    /**
     * Sets the value of the goods property.
     * 
     * @param value
     *     allowed object is
     *     {@link Taxcalculator.Goods }
     *     
     */
    public void setGoods(Taxcalculator.Goods value) {
        this.goods = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected         content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="good" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
     *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="isImported" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "good"
    })
    public static class Goods {

        @XmlElement(required = true)
        protected List<Taxcalculator.Goods.Good> good;

        /**
         * Gets the value of the good property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the good property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGood().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Taxcalculator.Goods.Good }
         * 
         * 
         */
        public List<Taxcalculator.Goods.Good> getGood() {
            if (good == null) {
                good = new ArrayList<Taxcalculator.Goods.Good>();
            }
            return this.good;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected         content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
         *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="isImported" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "name",
            "price",
            "description",
            "isImported"
        })
        public static class Good {

            @XmlElement(required = true)
            protected String name;
            protected double price;
            @XmlElement(required = true)
            protected String description;
            protected boolean isImported;

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the price property.
             * 
             */
            public double getPrice() {
                return price;
            }

            /**
             * Sets the value of the price property.
             * 
             */
            public void setPrice(double value) {
                this.price = value;
            }

            /**
             * Gets the value of the description property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Gets the value of the isImported property.
             * 
             */
            public boolean isIsImported() {
                return isImported;
            }

            /**
             * Sets the value of the isImported property.
             * 
             */
            public void setIsImported(boolean value) {
                this.isImported = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected         content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="salestax" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
     *         &lt;element name="inportedsalestax" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "salestax",
        "inportedsalestax"
    })
    public static class Taxrate {

        protected double salestax;
        protected double inportedsalestax;

        /**
         * Gets the value of the salestax property.
         * 
         */
        public double getSalestax() {
            return salestax;
        }

        /**
         * Sets the value of the salestax property.
         * 
         */
        public void setSalestax(double value) {
            this.salestax = value;
        }

        /**
         * Gets the value of the inportedsalestax property.
         * 
         */
        public double getInportedsalestax() {
            return inportedsalestax;
        }

        /**
         * Sets the value of the inportedsalestax property.
         * 
         */
        public void setInportedsalestax(double value) {
            this.inportedsalestax = value;
        }

    }

}

package com.liferay.tax.calculator.xml.gui;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GoodsStore extends Application {
	
    @Override
    public void start(Stage primaryStage) throws Exception{
    	//String fxmlFile = "file:///C:/Users/jeff/eclipse-workspace/LiferayCodeTask/src/main/java/com/liferay/tax/calculator/xml/gui/GoodsStore.fxml";
    	
    	//FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("/com/liferay/tax/calculator/xml/gui/GoodsStore.fxml"));
    	FXMLLoader loader = new FXMLLoader();
    	//loader.setLocation(new URL(fxmlFile));
    	//loader.setLocation(new URL("file:/com/liferay/tax/calculator/xml/gui/GoodsStore.fxml"));
    	loader.setLocation(getClass().getResource("/GoodsStore.fxml"));
    	Parent root = (Parent)loader.load();
    	
        primaryStage.setTitle("Goods Store");
        Scene scene = new Scene(root, 1200, 700);
        scene.setRoot(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
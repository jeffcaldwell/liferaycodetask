package com.liferay.tax.calculator;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.bind.JAXBException;
import com.liferay.tax.calculator.xml.jaxb.Taxcalculator;
import com.liferay.tax.calculator.xml.GoodsMarshaller;

public class TaxGoodsCalculator {
	private String projectDirectory = "C:\\Users\\jeff\\eclipse-workspace\\LiferayCodeTask";
	private File xsdfile = new File(projectDirectory + "\\src\\main\\java\\com\\liferay\\tax\\calculator\\xml\\xsd\\Goods.xsd");
	private File xmlfile = new File(projectDirectory + "\\Goods.xml");
	private Taxcalculator taxCalculator;
	private Reader xmlfileReader;
	private Reader xsdfileReader;
	private Reader xjbfileReader;
	
	public Taxcalculator getTaxCalculator() {
		if(taxCalculator == null) {
			loadResources();
			taxCalculator = loadTaxAndGoods();
		}
		return taxCalculator;
	}
	
	private void loadResources() {
		InputStream isxml = this.getClass().getClassLoader().getResourceAsStream("Goods.xml");
		xmlfileReader = new InputStreamReader(isxml);
		InputStream isxsd = this.getClass().getClassLoader().getResourceAsStream("Goods.xsd");
		xsdfileReader = new InputStreamReader(isxsd);
		InputStream isxjb = this.getClass().getClassLoader().getResourceAsStream("Goods.xjb");
		xjbfileReader = new InputStreamReader(isxjb);
	}
	
	private Taxcalculator loadTaxAndGoods() {
		Taxcalculator taxcalculator = null;
		GoodsMarshaller goodsMarshaller = new GoodsMarshaller();
		try {
			 taxcalculator = goodsMarshaller.unmarshall(xmlfileReader);
			 //taxcalculator = goodsMarshaller.unmarshall(xsdfile, xmlfile);
		} catch ( JAXBException e) {
			e.printStackTrace();
		}		
		return taxcalculator;
	}
}